package app.Exceptions;

public class DependencyCycleException extends Exception {

    public DependencyCycleException(String message)
    {
        super(message);
    }

}
