package app;


import app.Exceptions.DependencyCycleException;
import app.classes.A;

import java.io.IOException;

public class Main {

    public static void main(String[] args)
    {
        try {
            Scanner.scanClasses();
            Scanner.scanValues();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
