package app.classes;

import app.annotations.AttributeInjection;
import app.annotations.ConstructorInjection;
import app.annotations.LinkTo;
import app.annotations.SetterInjection;

public class C implements B{

    @AttributeInjection()
    @LinkTo(dependency = F.class)
    public F f = null;

    @SetterInjection()
    @Override
    public void setF(F f) {
        this.f = f;
    }

    @Override
    public F getF()
    {
        return f;
    }

    public C(){}

    @ConstructorInjection()
    public C(F f)
    {
        this.f = f;
    }
}
