package app.classes;

import app.annotations.*;

@ScanClass
public class E {

    @AttributeInjection()
    @LinkTo(dependency = F.class)
    public F f;

    public E(){}

    @ConstructorInjection()
    public E(F f)
    {
        this.f = f;
    }

    @SetterInjection()
    public void setF(F f){this.f = f;}
}
