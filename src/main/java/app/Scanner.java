package app;

import app.Exceptions.DependencyCycleException;
import app.annotations.LinkTo;
import app.annotations.ScanClass;
import org.reflections.Reflections;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

/**
 * Scan le code pour enregistrer les dépendances
 */
public class Scanner {

    //Enregistre les dépendances pour éviter les cycles
    private static Map<Class, Class> map = new HashMap<Class, Class>();

    /**
     * Scan les classes de app/classes pour enregistrer les dépendances
     * @throws DependencyCycleException : Exception : il y a un cycle
     */
    public static void scanClasses() throws DependencyCycleException {

        //Récupération des classes annotées
        Set<Class<?>> classes = new Reflections("app/classes").getTypesAnnotatedWith(ScanClass.class);

        //Reset de la map
        map.clear();

        //Pour toutes les classes
        for(Class c : classes)
        {
            //Enregistrer les dépendances
            registerClass(c);
        }

    }

    /**
     * Enregistre les dépendances d'une classe
     * @param c : la classe contenant les dépendances
     * @throws DependencyCycleException : Exception : il y a un cycle
     */
    protected static void registerClass(Class c) throws DependencyCycleException {

        //Récupération des attributs
        Field[] attributes = c.getDeclaredFields();

        //Pour tous les attributs
        for(Field f : attributes)
        {
            //Si l'attribut est annoté
            if(f.isAnnotationPresent(LinkTo.class))
            {
                //Récupération de l'annotation
                LinkTo link = f.getAnnotation(LinkTo.class);

                //Si il n'y a pas de cycle
                if(map.get(link.dependency()) == null)
                {
                    //Enregistrement de la dépendance
                    Container.register(f.getType(), link.dependency());
                    map.put(c, link.dependency());
                }
                else
                {
                    throw new DependencyCycleException("Cycle in dependency");
                }

            }
        }
    }

    /**
     * Enregistre les valeurs qui se trouvent dans values.properties
     * @throws IOException : file not found
     */
    protected static void scanValues() throws IOException {
        String configFileName = "values.properties";

        Properties prop = new Properties();

        InputStream inputStream = Scanner.class.getClassLoader().getResourceAsStream(configFileName);

        try
        {
            //Si inputstream est bien initialisé
            if (inputStream != null)
            {
                //Chargement du fichier de configuration
                prop.load(inputStream);
            }
            else
            {
                throw new FileNotFoundException("property file '" + configFileName + "' not found in the classpath");
            }

            //Récupération de toutes les valeurs
            Set keys = prop.keySet();

            //Pour chaque valeur
            for(Object key : keys)
            {
                try
                {
                    String value_str = prop.get(key).toString();
                    String[] tab = value_str.split(" ");

                    switch (tab[0])
                    {
                        case "double":
                            Container.registerValue(key.toString(), Double.parseDouble(tab[1]));
                            break;
                        case "int":
                            Container.registerValue(key.toString(), Integer.parseInt(tab[1]));
                            break;
                        case "string":
                            value_str = value_str.replace(tab[0] + " ", "");
                            Container.registerValue(key.toString(), value_str);
                            break;
                        case "char":
                            Container.registerValue(key.toString(), tab[1].charAt(0));
                            break;
                    }


                }catch (Exception e){}


            }

        } catch (Exception e) {
            System.out.println("Exception: " + e);
        } finally {
            inputStream.close();
        }

    }
}
