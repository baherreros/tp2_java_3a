package app;

import app.annotations.AttributeInjection;
import app.annotations.ConstructorInjection;
import app.annotations.SetterInjection;
import app.annotations.ValueInjection;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Stream;

/**
 * Injecteur de dépendance
 */
public class Container{

    //Contient les liens entre les classes et leurs interfaces
    static Map<Class, Class> _dependencies = new Hashtable<Class, Class>();

    //Contient les valeur enregistrées avec leur nom
    static Map<String, Object> _values = new Hashtable<String, Object>();

    /**
     * Enregistre les dépendances de classes
     * @param mother la classe mère (ou interface)
     * @param child la classe fille
     */
    static void register(Class mother, Class child)
    {
        //Si la mère est une interface et que la fille est différent et que la fille n'est pas une interface
        if(mother.isInterface() && mother != child && !child.isInterface())
        {
            //Check si la fille implémente l'interface mère
            Stream stream = Arrays.stream(child.getInterfaces());
            Boolean isMother = stream.anyMatch(x -> x == mother);

            if(isMother)
                _dependencies.put(mother, child);
        }
        //Si la fille hérite de la mère
        else if(child.isAssignableFrom(mother))
        {
            _dependencies.put(mother, child);
        }


    }

    /**
     * Instancie la classe avec ses dépendances à partir des setters
     * @param instance : la classe demandée
     * @param <T> : le type de la classe
     * @return : l'intance de la classe
     */
    static <T> T newInstanceWithSetters(Class<T> instance)
    {

        T t = null;

        try
        {
            //Nouvelle instance
            t = instance.newInstance();

            //Récupération des méthodes
            Method[] methods = instance.getMethods();

            //Pour toutes les méthodes
            for(Method m : methods)
            {
                //Si la méthode est annotée
                if(m.isAnnotationPresent(SetterInjection.class))
                {
                    //Récupération de la dépendance
                    Class dependency = _dependencies.get(m.getParameterTypes()[0]);

                    //Ajout dans la classe de manière recursive
                    m.invoke(t, newInstanceWithSetters(dependency));

                }
            }

            //Injection des valeurs
            injectValue(t);
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }

        return t;
    }

    /**
     * Instancie la classe avec ses dépendances à partir des constructeurs
     * @param instance : la classe demandée
     * @param <T> : le type de la classe
     * @return : l'intance de la classe
     */
    static <T> T newInstanceWithConstructor(Class<T> instance)
    {
        T t = null;

        try
        {
            //Nouvelle instance
            t = (T)instance.newInstance();

            //Récupération des constructeurs
            Constructor[] constructors = instance.getDeclaredConstructors();

            //Pour tous les constructeurs
            for(Constructor c : constructors)
            {
                //Si le constructeur est annoté
                if(c.isAnnotationPresent(ConstructorInjection.class))
                {
                    //Liste des arguments
                    List<Object> args = new ArrayList<>();

                    //Liste des types des arguments
                    Class[] types = c.getParameterTypes();

                    //Pour tous les arguments
                    for(Class value : types)
                    {
                        //Récupération de la dépendance
                        Class dependency = _dependencies.get(value);

                        //Ajout dans la liste des arguments de manière recursive
                        args.add(newInstanceWithConstructor(dependency));

                    }

                    //Appel au constructeur
                    t = (T) c.newInstance(args.toArray());
                }
            }

            //Injection des valeurs
            injectValue(t);
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }

        return t;
    }

    /**
     * Instancie la classe avec ses dépendances à partir des Attributs
     * @param instance : la classe demandée
     * @param <T> : le type de la classe
     * @return : l'intance de la classe
     */
    static <T> T newInstanceWithAttribute(Class<T> instance)
    {
        T t = null;

        try
        {
            //Nouvelle instance
            t = (T)instance.newInstance();

            //Récupération des attributs
            Field[] fields = instance.getDeclaredFields();

            //Pour tous les attributs
            for(Field f : fields)
            {
                //Si l'attribut est annoté
                if(f.isAnnotationPresent(AttributeInjection.class))
                {
                    //Modification de la visibilité
                    f.setAccessible(true);

                    //Récupération de la dépendance
                    Class dependency = _dependencies.get(f.getType());

                    //Set de la valeur de manière recursive
                    f.set(t, newInstanceWithAttribute(dependency));

                }
            }

            //Injection des valeurs
            injectValue(t);
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }

        return t;
    }

    /**
     * Permet d'enregistrer les valeurs avec leur nom
     * @param name : le nom de la valeur
     * @param value : la valeur
     */
    static void registerValue(String name, Object value )
    {
        if(value != null)
            _values.put(name, value);
    }

    /**
     * Permet de récupérer la valeur à partir de son nom
     * @param name : le nom de la valeur
     * @param <T> : le type de la valeur
     * @return : La valuer si elle existe ou null
     */
    static <T> T getValue(String name)
    {
        return (T)_values.get(name);
    }

    /**
     * Injecte les valeurs dans une classe
     * @param instance : la classe où il faut injecter les valeurs
     * @param <T> : le type de la classe
     */
    static <T> void injectValue(T instance)
    {
        try
        {
            //Récupération des attributs
            Field[] fields = instance.getClass().getDeclaredFields();

            //Pour tous les attributs
            for(Field f : fields)
            {
                //Si l'attribut est annoté
                if(f.isAnnotationPresent(ValueInjection.class))
                {
                    //Modification de la visibilité
                    f.setAccessible(true);

                    //Set de la valeur
                    f.set(instance, _values.get(f.getName()).getClass().cast(_values.get(f.getName())));
                }
            }
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }

    }
}
