package app;

import app.Exceptions.DependencyCycleException;
import app.classes.*;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Cycle
 *
 *      H --> I
 *      ^     |
 *      + --- +
 *
 */

/**
 *      +---> B --+
 * A ---|         +---> F ---> G
 *      +---> E --+
 *
 *      +-- C
 * B <--|
 *      +-- D
 */

public class TestScanner {

    @Test
    @DisplayName("Test Scanner")
    void testScanner()
    {

        try {
            Scanner.scanClasses();
        } catch (DependencyCycleException e) {
            e.printStackTrace();
        }

        assertEquals(4, Container._dependencies.size());
        assertEquals(E.class, Container._dependencies.get(E.class));
        assertEquals(F.class, Container._dependencies.get(F.class));
        assertEquals(G.class, Container._dependencies.get(G.class));
        assertEquals(D.class, Container._dependencies.get(B.class));


        //Test cycle
        Boolean h = false, i = false;

        try {
            Scanner.registerClass(H.class);
        } catch (DependencyCycleException e)
        {
            h = true;
        }

        assertFalse(h);

        try {
            Scanner.registerClass(I.class);
        } catch (DependencyCycleException e) {
            i = true;
        }

        assertTrue(i);
    }

    @Test
    @DisplayName("Test Scanner values")
    void testScannerValues()
    {

        try {
            Scanner.scanValues();
        } catch (IOException e) {
            e.printStackTrace();
        }

        assertEquals(4, Container._values.size());
        assertEquals(3.1415, (double)Container.getValue("pi"));
        assertEquals("une jolie propriété", (String) Container.getValue("name"));
        assertEquals(12, (int) Container.getValue("age"));
        assertEquals('c', (char) Container.getValue("char"));

    }

}
